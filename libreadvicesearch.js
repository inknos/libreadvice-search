function LibreadviceSearch(info) {
	var t = info.selectionText;
	t = t.trim();
	t = encodeURIComponent(t);
	chrome.tabs.create({
			url: "https://duckduckgo.com/?q="+t
	});
}

chrome.contextMenus.create({
	title: chrome.i18n.getMessage("title"),
	contexts:["selection"],
	onclick: LibreadviceSearch
});

