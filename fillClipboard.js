// First, ask the Permissions API if we have some kind of access to
// the "clipboard-read" feature.

var textElem = document.getElementById("input-search");
var pasteText = document.querySelector("#input-search");
pasteText.focus();
document.execCommand("paste");
textElem.select();


document.getElementById("fsf").onclick = function() {
    this.setAttribute("href", "https://directory.fsf.org/wiki?search="+encodeURI(textElem.value));
}
document.getElementById("libreadvice").onclick = function() {
    this.setAttribute("href", "https://duckduckgo.com/?q=site%3Alibreadvice.org+"+encodeURI(textElem.value));
}
document.getElementById("duckduckgo").onclick = function() {
    this.setAttribute("href", "https://duckduckgo.com/?q="+encodeURI(textElem.value));
}
document.getElementById("alternativeto").onclick = function() {
    this.setAttribute("href", "https://alternativeto.net/browse/search?license=opensource&q="+encodeURI(textElem.value));
}
/*
navigator.permissions.query({name: "clipboard-read"}).then(result => {
	// If permission to read the clipboard is granted or if the user will
	// be prompted to allow it, we proceed.

	if (result.state == "granted" || result.state == "prompt") {

		navigator.clipboard.read().then(clipText =>
		document.getElementById("input-search").value = clipText);

		navigator.clipboard.read().then(data => {
			for (let i=0; i<data.items.length; i++) {
				if (data.items[i].type != "text/plain") {
					textElem.value="NOT CLIPBOARD"
					alert("Clipboard contains non-text data. Unable to access it.");
				} else {
					textElem.value = data.items[i].getAs("text/plain");
				}
			}
		});
	}
});

*/

